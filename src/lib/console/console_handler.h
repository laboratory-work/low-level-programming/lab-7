#ifndef LLP_LAB_7_CONSOLE_HANDLER_H
#define LLP_LAB_7_CONSOLE_HANDLER_H

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <sysexits.h>

typedef struct {
    char* srcFile;
    char* distFile;
    int angle;

} attrConsole;

attrConsole analize(int argv, char** argc);

#endif //LLP_LAB_7_CONSOLE_HANDLER_H
