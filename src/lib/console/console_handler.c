#include "console_handler.h"

attrConsole analize(int argv, char** argc){
    attrConsole console = {};
    if(argv > 0){
        for(int i = 0; i < argv; i++) {
            if(strcmp(argc[i], "--help") == 0){
                fprintf(stdout, "");
                exit(0);
            }else if (strcmp(argc[i], "-s") == 0 && i+1 < argv) {
                console.srcFile = argc[i+1];
            }else if (strcmp(argc[i], "-d") == 0 && i+1 < argv) {
                console.distFile = argc[i+1];
            }else if (strcmp(argc[i], "-a") == 0 && i+1 < argv) {
                console.angle = atoi(argc[i + 1]);
            }
        }
        return console;
    } else {
        fprintf(stderr, "No found some arguments. Use --help to show command");
        exit(EX_USAGE);
    }
}