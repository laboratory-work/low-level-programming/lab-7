#include "bmp.h"

BMP* open(char* filename){
    BMP *bmp = readHeader(filename);
    if(bmp == NULL)
        return NULL;
    bmp->filename = filename;

    return bmp;
}

void read(BMP* bmp_file) {
    readPixels(bmp_file);
}

void write(BMP* bmp) {
    writeBMP(bmp);
}

void write_newFile(BMP* bmp_file, char* filename) {
    bmp_file->filename = filename;
    write(bmp_file);
}

int close(BMP* bmpFile) {
    _free(bmpFile->pixels.pixelData24Bit);
    _free(bmpFile);
    return 0;
}