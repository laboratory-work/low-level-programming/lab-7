#include "bmp_io.h"

BMP *createEmptyBMP() {
    BMP *newBMP = _malloc(1 * sizeof(BMP));
    newBMP->filename = NULL;
    newBMP->fileHeader = (BitmapFileHeader){};
    newBMP->infoHeader = (BitmapInfoHeader){};
    newBMP->colorTable = NULL;
    newBMP->pixels = (Pixels){};
    return newBMP;
}

BMP *copyEmptyBMP(BMP *bmpFile) {
    BMP *newBMP = _malloc(1 * sizeof(BMP));
    newBMP->fileHeader = bmpFile->fileHeader;
    newBMP->infoHeader = bmpFile->infoHeader;
    PixelData24Bit *pixels = _malloc(newBMP->infoHeader.biHeight * newBMP->infoHeader.biWidth * sizeof(PixelData24Bit));
    newBMP->pixels.pixelData24Bit = pixels;
    return newBMP;
}

BMP* readHeader(char *filename) {
    FILE *fp = fopen(filename, "rb");
    if (fp == NULL) {
        fprintf(stderr, "File %s is not opened", filename);
        exit(EX_IOERR);
    }

    BMP *bmp = createEmptyBMP();

    fread(&(bmp->fileHeader), sizeof(BitmapFileHeader), 1, fp);

    if (bmp->fileHeader.bfType != 0x4d42 && bmp->fileHeader.bfType != 0x4349 && bmp->fileHeader.bfType != 0x5450) {
        fprintf(stderr, "File %s is not BMP format", filename);
        fclose(fp);
        return NULL;
    }

    fseek(fp, 0, SEEK_END);
    long filesize = ftell(fp);
    fseek(fp, sizeof(BitmapFileHeader), SEEK_SET);

    fread(&(bmp->infoHeader), sizeof(BitmapInfoHeader), 1, fp);

    if(bmp->fileHeader.bfFileSize != filesize) {
        fprintf(stderr, "BMP file has invalid filesize field");
        fclose(fp);
        return NULL;
    }

    if(bmp->infoHeader.biSize != 40 && bmp->infoHeader.biSize != 108 && bmp->infoHeader.biSize != 124) {
        fprintf(stderr, "BMP file has unsupported size info header");
        fclose(fp);
        return NULL;
    }

    if(bmp->infoHeader.biWidth < 1 || bmp->infoHeader.biWidth > 10000 ||
            bmp->infoHeader.biHeight < 1 || bmp->infoHeader.biHeight > 10000) {
        fprintf(stderr, "BMP file has not supported width or height ( > 10000 or < 1)");
        fclose(fp);
        return NULL;
    }

    if (bmp->fileHeader.bfReserved != 0 || bmp->infoHeader.biPlanes != 1) {
        fprintf(stderr, "BMP file contains some errors in structure");
        fclose(fp);
        return NULL;
    }

    if (bmp->infoHeader.biCompression != 0) {
        fprintf(stderr, "BMP file is only not compressed");
        fclose(fp);
        return NULL;
    }

    return bmp;
}

void readPixels(BMP* bmp) {
    FILE *fp = fopen(bmp->filename, "rb");
    if (fp == NULL) {
        fprintf(stderr, "File %s is not opened", bmp->filename);
        exit(EX_NOINPUT);
    }

    fseek(fp, sizeof(BitmapFileHeader) + sizeof(BitmapInfoHeader), SEEK_SET);

    ColorTable *colorTable = NULL;
    if(bmp->infoHeader.biBitCount <= 8) {
        colorTable = malloc(bmp->infoHeader.biClrUsed * sizeof(ColorTable));
        for (size_t i = 0; i < bmp->infoHeader.biClrUsed; i++) {
            fread(&(colorTable[i]), sizeof(ColorTable), 1, fp);
        }
    }

    Pixels pixels = {};

    switch (bmp->infoHeader.biBitCount) {
        case 1:

            break;
        case 4:
            break;
        case 8:
            break;
        case 16:
            break;
        case 24:
            pixels.pixelData24Bit = malloc(bmp->infoHeader.biHeight * bmp->infoHeader.biWidth * sizeof(PixelData24Bit));
            if (pixels.pixelData24Bit == NULL) {
                fprintf(stderr, "Cannot create array of pixel");
                fclose(fp);
                exit(EX_OSERR);
            }

            int kr = (bmp->infoHeader.biWidth * 3) % 4;
            if (kr != 0) { kr = 4 - kr; }

            fseek(fp, bmp->fileHeader.bfOffBits, SEEK_SET);
            for (size_t i = 0; i < bmp->infoHeader.biHeight; i++) {
                for (size_t j = 0; j < bmp->infoHeader.biWidth; j++) {
                    size_t index = i * bmp->infoHeader.biWidth + j;
                    fread(pixels.pixelData24Bit + index, sizeof(PixelData24Bit), 1, fp);
                }
                for (int k = 0; k < kr; k++) { getc(fp); }
            }
            break;
        default:
            fprintf(stderr, "Unsupported BitCount");
            fclose(fp);
            exit(EX_DATAERR);
    }

    fclose(fp);

    bmp->colorTable = colorTable;
    bmp->pixels = pixels;
}

int writeBMP(BMP *bmp) {
    FILE *fw = fopen(bmp->filename, "w+");
    if (fw == NULL) {
        fprintf(stderr, "File %s is not opened", bmp->filename);
        exit(EX_CANTCREAT);
    }

    BitmapFileHeader fileHeader = bmp->fileHeader;
    BitmapInfoHeader infoHeader = bmp->infoHeader;

    fwrite(&(fileHeader), sizeof(BitmapFileHeader), 1, fw);
    fwrite(&(infoHeader), sizeof(BitmapInfoHeader), 1, fw);

    fseek(fw, fileHeader.bfOffBits, SEEK_SET);

    int kr = (infoHeader.biWidth * 3) % 4;
    if (kr != 0) { kr = 4 - kr; }

    PixelData24Bit *pixels = bmp->pixels.pixelData24Bit;
    for (size_t i = 0; i < infoHeader.biHeight; i++) {
        for (size_t j = 0; j < infoHeader.biWidth; j++) {
            size_t index = i * infoHeader.biWidth + j;
            fwrite(pixels + index, sizeof(PixelData24Bit), 1, fw);
        }
        for (int k = 0; k < kr; k++) { putc(0, fw); }
    }

    fclose(fw);
    return 0;
}