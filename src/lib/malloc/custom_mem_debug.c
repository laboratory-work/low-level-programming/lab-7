#include "custom_mem.h"

void mem_alloc_debug_struct_info(FILE* f, struct mem_t const* address) {
	size_t i;
	fprintf(f, "start: %p\nsize: %lu\nis_free: %d\nnext: %p\n",
		(void*)address, address->capacity, address->is_free, address->next);
	for (i = 0; i < DEBUG_FIRST_BYTES && i < address->capacity; ++i)
		fprintf(f, "%hhX", ((char*)address)[sizeof(struct mem_t) + i]);
	putc('\n', f);
}

void mem_alloc_debug_heap(FILE* f, struct mem_t const* ptr) {
	fprintf(f, "========start========\n");
	for (; ptr; ptr = ptr->next)
        mem_alloc_debug_struct_info(f, ptr);
	fprintf(f, "========end========\n");
}
